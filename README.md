# defance blog

## Installation

### Preparing and launching VM

```
curl https://bitbucket.org/defance/blog-configuration/raw/master/vagrant/Vagrantfile
vagrant up
```

### Inside VM

Checkout playbooks.

```
cd /blog/app/ansible/app/
git clone https://bitbucket.org/defance/blog-configuration.git ./
```

Install configuration requirements (ansible included).

```
source /blog/app/ansible/venv/ansible/bin/activate
sudo pip install -r requirements.txt
```

Launch provision.

```
ansible-playbook ./blog.yml
```

### Inside app dir

```
./manage.py createsuperuser
```